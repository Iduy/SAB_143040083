package com.example.yudi.sab_143040083;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity
{
    private Button btnSub1, btnSub2, btnDial;
    private String strIntent;
    private EditText txtIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSub1 = (Button)findViewById(R.id.btn_activity_modul1);
        btnSub2 = (Button)findViewById(R.id.btn_activity_modul2);

        txtIntent = (EditText)findViewById(R.id.text_Intent);

        btnSub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HitungLuas.class);
                startActivity(intent);
            }
        });

        btnSub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, modul2.class);
                startActivity(intent);
            }
        });

//        btnSub2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                strIntent = txtIntent.getText().toString();
//                Intent intent = new Intent(MainActivity.this, Sub2Activity.class);
//                intent.putExtra(Sub2Activity.KEY_DATA, strIntent);
//                startActivityForResult(intent, 0);
//            }
//        });


    }
}